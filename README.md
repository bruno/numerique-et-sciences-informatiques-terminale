# Numérique et Sciences Informatiques - Terminale

Les cours de NSI de terminale du lycée l'Emperi 2023-2024 sont déposés et générés ici.

Site de rendu du dépôt : [https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-terminale/](https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-terminale/)