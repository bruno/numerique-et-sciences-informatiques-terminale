---
author: Bruno Bourgine
title : Interfaces et implémentations
hide :
 - footer
---


Lorsque l'on souhaite manipuler une donnée de manière efficace, il s'agit 
tout d'abord de définir les opérations que l'on souhaite réaliser dessus, 
pour ensuite les implémenter. Nous verrons dans ce chapitre comment appréhender 
une structure de données du point de vue de son utilisation ainsi que du point 
de vue de sa programmation.

## 1. Définitions

!!! info "Structure de données" 

    Organisation d'une collection de données en vue de leur exploitation efficace 
    (accès, modification, ...) à laquelle on associe un ensemble d'opérations qu'on 
    peut leur appliquer.

!!! info "Interface"

    L'interface représente l'ensemble des méthodes dont l'utilisateur dispose afin
    de manipuler les données. Elle spécifie la nature des données ainsi que l'ensemble 
    des opérations permises sur la structure.

!!! info "Implémentation"

    Il s'agit de la programmation effective des opérations définies dans l'interface, 
    en utilisant des types de données existants. On dit que l'implémentation *réalise*
    l'interface. 


!!! example "Métaphore ..."

    On peut choisir comme image une machine à café à capsule, dans laquelle on peut 
    distinguer :  


    - l' **INTERFACE** : les boutons, les voyants, le levier…  
    - l' **IMPLÉMENTATION** : les électrovannes, la pompe, le bloc de chauffe, voire le 
    déroulement des opérations
  
    L'utilisateur n'a pas besoin de savoir comment fonctionne la machine à l'intérieur 
    (*implémentation*), pour se faire un café (*interface*)...


    === "Interface"

        ![interface_cafe](res/illustration_interface.jpg){width=200px}

    === "Implémentation"

        ![implémentation_cafe](res/illustration_implementation.jpg){width=200px}

!!! note "En résumé"

    L'*interface* est la partie visible pour qui veut utiliser un type de structure 
    données. Elle précise comment utiliser la structure de données sans se préoccuper 
    de la façon dont les choses ont été programmées (son *implémentation*).

## 2. Exemple : la structure *Rationnel*

### 2.1 Interface de la structure de données

On aimerait définir une **structure de données** appelée `Rationnel` correspondant
à l'ensemble des nombres rationnels (noté $\mathbb Q$). Voici les opérations que 
l'on souhaite effectuer sur les rationnels :

- ***Créer*** un rationnel
- ***Accéder*** au numérateur et au dénominateur d'un rationnel
- ***Ajouter***, ***soustraire***, ***multiplier***, ***diviser*** deux rationnels
- ***Vérifier*** si deux rationnels sont égaux ou non

On spécifie l'ensemble des opérations souhaitées en proposant l'**interface** suivante :

- `rationnel_creer(numerateur : int , denominateur : int) -> Rationnel` : crée un élément 
de type `Rationnel` à partir de deux entiers  `n` (numérateur) et `d` (dénominateur). 
*Précondition* : $d \neq 0$.  
- `rationnel_get_numerateur( r : Rationnel) -> int` : accès au numérateur du rationnel 
`r` (renvoie un entier)  
- `rationnel_get_denominateur( r : Rationnel) -> int` : accès au dénominateur du rationnel 
`r` (renvoie un entier non nul)  
- `rationnel_ajouter(r1 : Rationnel, r2 : Rationnel) -> Rationnel` : renvoie un nouveau 
rationnel correspondant à la somme des rationnels `r1` et `r2`  
- `rationnel_soustraire(r1 : Rationnel, r2 : Rationnel) -> Rationnel` : renvoie un nouveau 
rationnel correspondant à la différence des rationnels `r1` et `r2` (`r1` -`r2`)  
- `rationnel_multiplier(r1 : Rationnel, r2 : Rationnel) -> Rationnel`` : renvoie un nouveau 
rationnel correspondant au produit des rationnels `r1` et `r2`  
- `rationnel_est_egal(r1: Rationnel, r2 : Rationnel) -> bool` : renvoie Vrai si les deux 
rationnels `r1` et `r2` sont égaux, Faux sinon.  

On ajoute à cela une opération permettant d'afficher un rationnel sous la forme d'une chaîne 
de caractères :

- `rationnel_afficher(r : Rationnel) -> None` : affiche le rationnel `r` sous la forme d'une 
chaîne de caractères `'n/d'` où `n` et `d` sont respectivement le numérateur et le dénominateur 
de `r`.

### 2.2 Exemple d'utilisation de la structure

L'interface apporte toutes les informations nécessaires pour utiliser le type de données. 
Ainsi, le programmeur qui l'utilise n'a pas à se soucier de la façon dont les données sont 
représentées ni de la manière dont les opérations sont programmées. L'interface (uniquement) 
lui permet d'écrire toutes les instructions qu'il souhaite et obtenir des résultats corrects. 

Par exemple, il sait qu'il peut écrire le programme suivant pour manipuler le type `Rationnel` 
(écrit ici en Python mais on pourrait le faire dans un autre langage).

```Python
r1 = rationnel_creer(1, 2)
den = rationnel_get_denominateur(r1)  # den vaut donc 2
r2 = rationnel_creer(1, 3*den)  # r2 représente le rationnel 1/6
r = rationnel_ajouter(r1, r2)  # r est le résultat de 1/2 + 1/6
rationnel_est_egal(r, rationnel_creer(2, 3)) # doit renvoyer True puisque 1/2 + 1/6 = 2/3
```

### 2.3 Implémentations

Pour implémenter une structure de données, c'est-à-dire programmer les opérations 
vont réaliser cette interface, on va utiliser les structures de données nativement
définies dans le langage utilisé.

En Python, on peut avantageusement utiliser les types `int`, `float`, `boolean` 
ainsi que les tuples (avec le type `tuple`), les tableaux (avec le type `list`), 
les dictionnaires (avec le type `dict`), les ensembles (avec le type `set`).

Nous allons étudier deux implémentations : l'une avec un tuple (on aurait pu utiliser 
un tableau de manière identique) et l'autre avec un dictionnaire.

#### Une implémentation possible

On peut par exemple implémenter (= *programmer* concrètement) le type `Rationnel` en utilisant 
des couples (le type `tuple` de Python). Voici ce que pourrait alors être l'implémentation 
de certaines des opérations du type `Rationnel`.

!!! warning "Remarque"

    Le propos étant ici l'implémentation, la documentation des fonctions n'a volontairement pas 
    été incluse afin d'alléger le code. Néanmoins, il est évident que la documentation
    est un élément indispensable de l'interface.

{{ IDE ('scripts/implementation')}}

On peut alors vérifier, même si on le savait déjà grâce à l'interface, que les instructions 
précédentes donnent des résultats corrects (on a pris le soin d'afficher certains résultats 
pour illustrer).

{{ IDEv('scripts/verif_interface') }}


#### Une autre implémentation possible

Imaginons que le programmeur qui a implémenté le type abstrait `Rationnel` ait fait des 
choix différents :  

- il a utilisé des dictionnaires pour représenter les rationnels  
- il a choisi de ne pas simplifier les fractions au fur et à mesure des calculs, ce qui 
  implique une autre écriture du test d'égalité  

{{ IDE ('scripts/implementation2') }}

On peut vérifier que l'on peut écrire exactement les mêmes instructions que précédemment 
et obtenir exactement les mêmes résultats, alors même que l'implémentation est totalement 
différente.

{{ IDEv ()}}


!!! tldr "Bilan de l'exemple"

    On a bien vu que le programmeur qui utilise une structure de données fait *abstraction* 
    à la fois : 

    - de la manière dont les données sont représentées (ex. : l'écriture des instructions et 
    les résultats sont les mêmes, que les données soient représentées par des couples ou des 
    dictionnaires, ou autre chose...)   
    - de la manière dont les opérations sont programmées (ex. : le test d'égalité ne suit pas 
    la même logique selon les deux implémentations mais le résultat est le même).  

    Il n'est pas nécessaire de connaître l'implémentation pour manipuler la structure de données


## 3. Structures de données abstraites

On parle de *structure de données abstraites (SDA)* ou de *type abstrait de données (TAD)* car 
au niveau de l'interface les données, leurs liens et les opérations permises sont précisées 
mais on ne sait pas (et on ne veut pas savoir) comment c'est fait concrètement (implémentation).

Les types abstraits permettent donc de définir des types de données *non primitifs*, c'est-à-dire 
non disponibles dans les langages de programmation courants. Les types *primitifs* sont par exemple 
les entiers, les flottants, les booléens.

### 3.1 Quelques structures de données abstraites

Les structures de données abstraites que nous étudierons cette année peuvent être classées selon 
la nature de l'organisation de la collection de données :

- **structures linéaires** (ou **séquentielles**) : il y a un premier élément et un dernier ; 
chaque élément a un prédécesseur (sauf le premier) et un successeur (sauf le dernier). Exemples :
**liste**, **file**, **pile**.  
- **structures associatives** : les éléments sont repérés par une *clé* ; ils n'ont pas de lien 
entre eux. Exemples : **dictionnaires**, **ensembles**.  
- **structures hiérarchiques** : il y a un élément *racine* ; chaque élément dépend d'un *antécédent* 
(sauf la racine) et a des *descendants* (sauf les feuilles). Exemple : **arbre**.  
- **structures relationnelles** : chaque élément est en relation directe avec des *voisins*, ou 
bien a des prédécesseurs et des successeurs. Exemple : **graphe**.  

!!! note ""

    Certaines structures sont nativement implémentées dans les langages de programmation, 
    comme par exemple les dictionnaires `dict` et les ensembles `set` en Python.

### 3.2 Opérations des structures de données abstraites

Les opérations usuelles d'une structure de données abstraite sont :

 - **Créer** une donnée (éventuellement vide), en utilisant ce qu'on appelle un *constructeur*.  
 - **Accéder** à un élément (soit directement, soit à partir de son prédécesseur ou successeur).  
 - **Ajouter** un élément, en précisant comment il s'intègre dans la structure.  
 - **Retirer** un élément, en précisant comment ceux qui lui étaient liés se réorganisent.  
 - Eventuellement, des opérations plus avancées (rechercher un élément, trier la collection,
  fusionner deux collections, ...)

Chaque opération doit être bien spécifiée (*entrée*, *sorties*, *précondition(s)*) : c'est ce qui 
est détaillé par l'interface.

## 4. Synthèse

- Une **structure de données** est une méthode de stockage et d'organisation des données 
destinée à en faciliter l'accès et la modification. Elle regroupe des *données* à gérer 
et un *ensemble d'opérations* qu'on peut leur appliquer.  
- Les structures de données s'envisagent à deux niveaux : l'*interface* (abstrait) et 
l'*implémentation* (concret).  
    - L'**interface** est la spécification de l'ensemble des opérations de la structure de 
    données. C'est la partie visible pour qui veut utiliser ce type abstrait de données. Elle 
    est suffisante pour utiliser la structure de données.  
    - L'**implémentation** consiste à *concrétiser* (à réaliser effectivement) un type de 
    données en définissant la représentation des données avec des types de données existants, 
    et en écrivant les programmes des opérations. L'utilisateur doit pouvoir écrire les mêmes 
    instructions et obtenir les mêmes résultats quelle que soit l'implémentation de la structure 
    de données.  
- On peut écrire plusieurs implémentations d'une même structure de données (d'une même interface). 
Le choix de l'implémentation est en général guidé par les opérations de la structure et leurs 
coûts pour un traitement algorithmique donné. 

--- 

références :

- [Germain BECKER, Lycée Mounier, ANGERS](https://info-mounier.fr/terminale_nsi/structures_donnees/index.php#chap1)
- [eskool nsi terminale](https://eskool.gitlab.io/tnsi/donnees/)