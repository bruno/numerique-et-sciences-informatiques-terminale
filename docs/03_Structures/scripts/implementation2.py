Rationnel = dict[str, int]

def rationnel_creer(numerateur : int, denominateur : int) -> Rationnel:
    return {"num": numerateur, "den": denominateur}

def rationnel_get_denominateur(r: Rationnel) -> int:
    return r["den"]

def rationnel_ajouter(r1 : Rationnel, r2 : Rationnel) -> Rationnel:
    num = r1["num"] * r2["den"] + r2["num"] * r1["den"]
    den = r1["den"] * r2["den"]
    return rationnel_creer(num, den)

def rationnel_est_egal(r1 : Rationnel, r2 : Rationnel) -> bool:
    return r1["num"] * r2["den"] == r2["num"] * r1["den"] 

def rationnel_afficher(rationnel : Rationnel) -> None :
    print(f"{rationnel['num']}/{rationnel['den']}")