# création d'un dictionnaire vide
d = {}
# ou 
d = dict()

# ajout de nouveaux couples clé-valeur
d['a'] = 2
print(d)   # affiche {'a': 2}
d['b'] = 5
d['c'] = 3
print(d)  # affiche {'a': 2, 'b': 5, 'c': 3}

# modification d'une valeur associée à une clé
d['a'] = 1
print(d)  # affiche {'a': 1, 'b': 5, 'c': 3}

# suppression d'un couple clé-valeur
del d['b']
print(d)  # affiche {'a': 1, 'c': 3}

# récupération de la valeur associée à une clé donnée
valeur = d['c']
print(valeur)  # affiche 3