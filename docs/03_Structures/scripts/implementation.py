Rationnel = tuple[int, int]

def rationnel_creer(numerateur : int , denominateur : int) -> Rationnel:
    assert denominateur != 0
    return (numerateur, denominateur)

def rationnel_get_denominateur( r : Rationnel) -> int:
    return r[1]

def rationnel_ajouter(r1 : Rationnel, r2 : Rationnel) -> Rationnel:
    numerateur = r1[0] * r2[1] + r2[0] * r1[1]
    denominateur = r1[1] * r2[1]
    # simplification du r en divisant le numerateurérateur et 
    # le dénominateur par leur pgcd
    diviseur = pgcd(numerateur, denominateur)
    return rationnel_creer(numerateur // diviseur, denominateur // diviseur)

def rationnel_est_egal(r1 : Rationnel, r2 : Rationnel) -> bool :
    return r1 == r2

def rationnel_afficher(r : Rationnel) -> None:
    print(f"{r[0]}/{r[1]}")

# Une fonction ne faisant pas partie de l'interface de la 
# structure de données mais utilisée dans la fonction ajouter
def pgcd(a :int, b : int) -> int:
    """Renvoie le pgcd des entiers positifs a et b"""
    return a if b == 0 else pgcd(b, a) if b > a else pgcd (a-b, b)