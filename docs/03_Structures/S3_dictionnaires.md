---
title : Dictionnaires
hide :
 - footer
---

## 1. Tableaux associatif


Un tableau associatif est un type abstrait de données (commes les piles, 
les files, les listes, les arbres, etc.).

Ce type abstrait n'est pas linéaire car il associe des **valeurs** à des **clés**.

!!! info "Opérations de base"

    Les opérations usuelles du type *Tableau associatif* sont :

    - création d'un dictionnaire vide;  
    - ajout d'une nouvelle valeur associée à une nouvelle clé (on parlera de 
     nouveau couple clé-valeur);  
    - modification d'une valeur associée à une clé existante;  
    - suppression d'un couple clé-valeur;  
    - récupération de la valeur associée à une clé donnée.  

Exemple : un répertoire téléphonique est un exemple de tableau associatif.

Un répertoire téléphonique est un exemple de tableau associatif :

- les clés sont les noms
- les valeurs sont les numéros de téléphone


En Python, le dictionnaire est une structure native de tableau associatif. Les dictionnaires 
Python possèdent ces 5 opérations (et d'autres évidemment) :

!!! example "Exemples de manipulation de base"

{{ IDE('scripts/dico1') }}

## 2. Temps d'accès aux valeurs

En Python, pour tester si une valeur est présente parmi les clés d'un dictionnaire, 
on peut utiliser le mot clé `in`, tout comme pour les types `list`, `str`, `tuple` ... :

!!! example "Recherche dans un dictionnaire"
    
    Utiliser la console de l'IDE ci-dessous afin de rechercher l'existence de 'a' et de 'e' 
    dans le dictionnaire de l'exemple précédemment (lancer le script vide au préalable).

    {{ IDEv()}}

Cette recherche est-elle comparable à celle d'un élément dans un tableau ou une liste 
chaînée ? Nous allons procéder à quelques comparaisons...

### 2.1 Protocole

!!! info "Principe"

    On veut mesurer le temps d'accès à une valeur dans un dictionnaire, et le 
    comparer à celui permettant d'accéder à un élément dans un tableau. 
    
    Pour cela, on va se placer dans le pire cas : chercher une valeur absente 
    (du dictionnaire et du tableau).

!!! example "Méthodes de comparaison"

    Le code ci-dessous, comporte les éléments suivants :

    - La fonction `mesures`, qui prend en paramètre un nombre `nb` qui sera la taille 
    du tableau ou du dictionnaire. Dans le corps de cette fonction, le tableau `tab` 
    et le dictionnaire `d` sont fabriqués avant le commencement de la mesure du temps.

    - Le tableau `tab`, qui contient des nombres (de 1 à `nb`), et le dictionnaire `d` 
    qui associe à un nombre (de 1 à nb) sa propre valeur.

    Dans ces deux structures, nous allons partir à la recherche d'une valeur qui n'a aucune chance de s'y trouver : la chaine de caractères `a`.

    On effectue la recherche 10 fois de suite (pour avoir un temps moyen le plus juste 
    possible), on va donc mesurer le temps mis pour chercher la chaine `a`, qui n'est présente
    ni dans le tableau ni dans le dictionnaire `d`.
    
    On mesure donc une recherche dans le pire  des cas. 

    Lancer le script ci-dessous :

    {{ IDE('scripts/dico2')}}

### 2.2 Mesures

En effectuant 4 mesures, pour des tailles allant de 10<sup>4</sup> à 10<sup>7</sup>, on
constate que :

- le temps de recherche dans une liste augmente d'un facteur 10 lorsque la taille de 
    la liste augmente d'un facteur 10.   
- le temps de recherche dans un dictionnaire reste dans le même ordre de grandeur, proche de 0 seconde, et ce quelle que soit la taille du dictionnaire.
  
Vous pouvez le tester vous-même en appelant la fonction `mesures` dans la console
précédente avec les valeurs proposées (au delà de 10<sup>7</sup>, cela provoquera
une erreur).

```python
>>> mesures(10**4)
>>> mesures(10**5)
>>> mesures(10**6)
>>> mesures(10**7)
```

### 2.3. Conclusion

Il y a donc une différence fondamentale entre les temps de recherche d'un élément dans un 
tableau et dans un dictionnaire.

!!! warning "Différence de temps de parcours entre tableau et dictionnaire"

    - dans un tableau, le coût en temps est linéaire, c'est-à-dire qu'il est proportionnel à la taille $n$ du tableau : on écrit que la recherche dans un tableau (ou une liste) est en 
     $O(n)$.  
    - dans un dictionnaire, le coût en temps est constant, c'est-à-dire qu'il ne dépend pas de la taille du dictionnaire : on écrit que la recherche dans un dictionnaire est en $O(1)$.

Ainsi, si on sait à l'avance que l'on va devoir chercher régulièrement des valeurs dans une 
structure de données, l'utilisation d'un dictionnaire sera beaucoup plus efficace en temps que 
pour un tableau (à condition que ces structures soient adaptées au stockage de nos données bien 
sûr).

## 3. Hachage  (hors programme)

Il est important de se rappeler qu'un dictionnaire **n'est pas ordonné** (contrairement à 
l'objet «dictionnaire» de la vie courante, où chaque mot est classé suivant l'ordre 
alphabétique).

On n'accède pas à une valeur suivant sa position, mais suivant sa **clé**.

Dans une liste, lorsqu'on veut savoir si un élément appartient à une liste (problème de la 
recherche d'élément), il n'y a pas (dans le cas général) de meilleure méthode que le parcours 
exhaustif de tous les éléments de la liste jusqu'à (éventuellement) trouver la valeur cherchée.

Dans un dictionnaire, on pourrait s'imaginer qu'il va falloir parcourir toutes les clés et 
regarder les valeurs correspondantes. Il n'en est rien.

Pour comprendre cela nous allons faire un petit détour par les *fonctions de hachage*.

### 3.1 Fonction de hachage

Une fonction de hachage est une fonction qui va calculer une empreinte unique à partir de la donnée fournie en entrée. Elle doit respecter les règles suivantes :

- la longueur de l'empreinte (valeur retournée par la fonction de hachage) doit être toujours la même, indépendamment de la donnée fournie en entrée;  
- connaissant l'empreinte, il ne doit pas être possible de reconstituer la donnée d'origine;  
- des données différentes doivent donner dans la mesure du possible des empreintes différentes;  
- des données identiques doivent donner des empreintes identiques.

!!! example "MD5"

    La fonction de hachage md5 permet de convertir un mot binaire (une chaîne, un fichier, ...) de taille quelconque en un mot de 128 bits représenté par une chaîne hexadécimale de 32 caractères (il y a donc 2128≃1039 empreintes MD5 différentes).

    ![](https://upload.wikimedia.org/wikipedia/commons/5/51/Hachage.svg)

    *Unique Nitrogen*, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons

!!! info "Remarques"

  - Le mécanisme de calcul de la fonction MD5 est complexe, pour en savoir plus allez voir 
  cette [page](https://www.bibmath.net/crypto/index.php?action=affiche&quoi=moderne/md5).  
  - Il est évidemment impossible de revenir en arrière, sinon on serait capable de compresser 
  sans perte n'importe quel fichier en une chaîne de 128 bits. Cette impossibilité de trouver 
  une fonction réciproque à la fonction de hachage est très important en cryptographie.
  - La fonction MD5 n'est plus utilisée depuis 2005 pour des applications sensibles car des 
  chercheurs ont prouvé qu'il était possible de créer facilement deux messages ayant la même 
  empreinte, ce qui contredit la troisième règle donnée au-dessus.  
  - Aujourd'hui, on utilise principalement la fonction de hachage SHA-256.

### 3.2 Utilisations concrètes de fonctions de hachage

#### Vérifier l'intégrité d'un fichier

Lorsque vous téléchargez un fichier important et que vous souhaitez vérifier qu'il n'a pas 
été corrompu lors du téléchargement (ou avant), vous avez parfois la possibilité de vérifier 
l'intégrité de votre fichier téléchargé, en calculant une «empreinte» de votre fichier et en 
la comparant avec celle que vous êtes censée obtenir.

#### Stockage des mots de passe

L'utilisation la plus courante est le stockage des mots de passe dans un système informatique 
un peu sécurisé. En effet, lorsqu'on crée un compte sur un service en ligne, le mot de passe ne 
doit pas être stocké en clair, une empreinte est générée (le hash du mot de passe) et c'est ce 
*hash* du mot de passe qui est stocké sur le serveur. Cela permet d'éviter, en cas de piratage, 
de protéger les comptes car il n'est pas possible de reconstituer les mots de passe à partir et 
des empreintes.

### 3.3 Retour aux dictionnaires Python

!!! question "Quel est le lien entre les fonctions de hachage et les dictionnaires ?"

    L'idée essentielle est que chaque clé est hachée pour donner une empreinte unique, 
    qui est ensuite transformée en un indice de positionnement dans un tableau.

!!! example ""
    
    Le dictionnaire :

    ```python
    d = {"pommes": 3, "poires": 0, "bananes": 5}
    ```
    serait donc par exemple implémenté dans un tableau comme celui-ci :

    ![](https://info-mounier.fr/terminale_nsi/structures_donnees/data/hashdico.png)

    Si je souhaite ensuite accéder à l'élément `d["kiwis"]` :

    - le hash de la chaîne "kiwis" est calculé. Par exemple, 4512d2202.  
    - l'indice de la position (éventuelle) de la clé "kiwis" dans mon dictionnaire est 
    calculé à partir de ce hash 4512d2202. Dans notre exemple, cela pourrait donner 
    l'indice 3.  
    - Python accède directement à cet indice du tableau :  
      - si la valeur de la clé sur cette ligne du tableau est None, cela signifie que 
      "kiwis" n'est pas une clé existante du tableau. C'est notre cas ici car il n'y a 
      rien à la ligne 3.  
      - si la valeur de la clé sur cette ligne du tableau est bien "kiwis", la valeur 
      correspondante est renvoyée.

En résumé, Python sait toujours où aller chercher un élément de son dictionnaire : soit il 
le trouve à l'endroit calculé, soit il n'y a rien à cet endroit calculé, ce qui veut dire que
 l'élément ne fait pas partie du dictionnaire.

Par ce mécanisme, l'accès à un élément du dictionnaire se fait toujours en **temps constant**.

!!! warning "Type des clés des dictionnaires"

    Une conséquence de l'utilisation du hashage pour implémenter un dictionnaire est
    que les clés doivent être des valeurs immuable. En effet un changement de valeur 
    tout en gardant la même référence détruirait le principe associant à une clé unique 
    une position unique dans le tableau implémentant le dictionnaire.

    C'est pourquoi notamment il n'est pas possible d'utiliser une donnée de  type `list` 
    comme clé de dictionnaire.

!!! quote "Références"

    - [Gilles Lassus, Lycée François Mauriac, Bordeaux](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.2_Dictionnaires/cours/)
    - [Germain BECKER, Lycée Mounier, Angers](https://info-mounier.fr/terminale_nsi/structures_donnees/dictionnaires.php)