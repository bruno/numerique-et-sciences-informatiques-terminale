---
title : Graphes
hide :
 - footer
---


## 1. Notion de graphe et vocabulaire

Le concept de graphe permet de résoudre de nombreux problèmes en mathématiques comme en informatique. C'est un outil de représentation très courant, et nous l'avons déjà rencontré à plusieurs reprises, en particulier lors de l'étude de réseaux.


!!! note "Exemples de situations" 
    === "Réseau informatique"
        ![](res/graphe-reseau.png){: .center width=640}

    === "Réseau de transport"
        ![](res/carte-metro-parisien-768x890.jpg){: .center width=640} 

    === "Réseau social"
        ![](res/graphe_RS.png){: .center width=640} 

    === "Mais aussi"
        On trouve également des applications de la théorie des graphes dans bien d'autres domaines: probabilités, biologie, physique, chimie, génétique...

!!! abstract "Vocabulaire des graphes"
    En général, un graphe est un ensemble d'objets, appelés *sommets* ou parfois *nœuds* (*vertex* or *nodes* en anglais) reliés par des *arêtes* ou *arcs* selon que le graphe est **non orienté** ou **orienté** (*edge* en anglais).

    === "Graphe non orienté"
        ![](res/exemple_graphe.png){: .center width=480} 

        Dans un graphe **non orienté**, les *arêtes* peuvent être empruntées dans les deux sens, et une *chaîne* est une suite de sommets reliés par des arêtes, comme C - B - A - E par exemple. La *longueur* de cette chaîne est alors 3, soit le nombre d'arêtes.

        Les sommets B et E sont *adjacents* au sommet A, ce sont les *voisins* de A.

    === "Graphe orienté"
        ![](res/exemple_graphe_oriente.png){: .center width=480} 

        Dans un graphe **orienté**, les *arcs* ne peuvent être empruntés que dans le sens de la flèche, et un *chemin* est une suite de sommets reliés par des arcs, comme B → C → D → E par exemple.

        Les sommets C et D sont *adjacents* au sommet B (mais pas A !), ce sont les *voisins* de B.

    === "Graphe pondéré"
        ![](res/exemple_graphe_pondere.png){: .center width=480} 

        Un graphe est **pondéré** (ou valué) si on attribue à chaque arête une valeur numérique (la plupart du temps positive), qu'on appelle *mesure*, *poids*, *coût* ou *valuation*.
        
        Par exemple:
        
        - dans le protocole OSPF, on pondère les liaisons entre routeurs par le coût;
        - dans un réseau routier entre plusieurs villes, on pondère par les distances.

!!! abstract "Connexité"
    Un graphe est **connexe** s'il est d'un seul tenant: c'est-à-dire si n'importe quelle paire de sommets peut toujours être reliée par une chaîne. Autrement un graphe est connexe s'il est «en un seul morceau».

    Par exemple, le graphe précédent est connexe. Mais le suivant ne l'est pas: il n'existe pas de chaîne entre les sommets A et F par exemple.

    ![](res/exemple_graphe_non_connexe.png){: .center width=480} 

    Il possède cependant deux **composantes connexes** : le sous-graphe composé des sommets A, B, C, D et E d'une part et le sous-grpahe composé des sommets F, G et H.


## 2. Modélisations d'un graphe

Nous allons voir les deux principales façon de représenter un graphe, c'est-à-dire par les sommets et leurs arêtes sortantes vers leur voisins (d'où le terme d'**adjacence**).

### 2.1 Représentation par matrice d'adjacence

!!! abstract "Principe"
    - On numérote les sommets de 0 à $n-1$.
    - on représente les arêtes (ou les arcs) dans une matrice, c'est-à-dire un tableau à deux dimensions où on inscrit un 1 en ligne `i` et colonne `j` si les sommets `i` et `j` sont voisins.
    
    === "Graphe et matrice"

        ![](res/graphe_exemple1.png){: .center width=320}
        ![](res/matrice_exemple1.png){: .center width=320} 

    === "Arête reliant deux sommets"

        ![](res/graphe_exemple1b.png){: .center width=320}
        ![](res/matrice_exemple1b.png){: .center width=320} 


!!! info "Remarques"
    - La matrice d'adjacence d'un graphe **non orienté** est symétrique.
    - Pour un graphe pondéré, on indique la *valuation* de l'arête plutôt qu'un 1.
    - Pour un graphe à $n$ sommets, la complexité spatiale (place en mémoire) est en $O(n^2)$.
    - Tester si un sommet est isolé (ou connaître ses voisins) est en $O(n)$ puisqu'il faut parcourir une ligne, mais tester si deux sommets sont adjacents (voisins) est en $O(1)$, c'est un simple accès au tableau.


!!! code "En Python"
    Une matrice se représente naturellement par une liste de listes.

    **Exemple:**

    ```python linenums='1'
    mat = [[0, 1, 1, 0, 0],
           [1, 0, 1, 1, 0],
           [1, 1, 0, 1, 1],
           [0, 1, 1, 0, 1],
           [0, 0, 1, 1, 0]] 
    ```
    
### 2.2 Représentation par listes d'adjacence


!!! abstract "Principe"
    - On associe à chaque sommet sa liste des voisins (c'est-à-dire les sommets adjacents). On utilise pour cela un dictionnaire dont les clés sont les sommets et les valeurs les listes des voisins.

    - Dans le cas d'un graphe **orienté** on associe à chaque sommet la liste des *successeurs* (ou bien des *prédécesseurs*, au choix).

    Par exemple, le graphe précédent s'écrira en Python:

    ```python linenums='1'
    G = {0: [1, 2],
         1: [0, 2, 3],
         2: [0, 1, 3, 4],
         3: [1, 2, 4],
         4: [2, 3]
        }
    ```
    
!!! info "Remarques"
    - Pour un graphe pondéré, on associera un dictionnaire (d'associations `#!py voisin: valuation`).
    - Pour un graphe à $n$ sommets et $m$ arêtes, la complexité spatiale (place en mémoire) est en $O(n+m)$. C'est beaucoup mieux qu'une matrice d'adjacence lorsque le graphe comporte peu d'arêtes (i.e. beaucoup de 0 dans la matrice, non stockés avec des listes).
    - Tester si un sommet est isolé (ou connaître ses voisins) est en $O(1)$ puisqu'on y accède immédiatement, mais tester si deux sommets sont adjacents (voisins) est en $O(n)$ car il faut parcourir la liste.

### 2.3 Exercices

!!! example "Exercice 1"
    === "Énoncé" 
        Construire les représentations des graphes suivants:

        1. Par matrice d'adjacence.
        2. Par listes d'adjacence.

        ![](res/exemple_graphe.png){: .center width=240} 

        ![](res/exemple_graphe_oriente.png){: .center width=240} 

        ![](res/exemple_graphe_pondere.png){: .center width=240} 

    === "Correction" 
        

!!! example "Exercice 2"
    === "Énoncé" 
        1. Construire les graphes correspondants aux matrices d'adjacences suivantes:

            $M_1 =\pmatrix{
                0&1&1&1&1\\
                1&0&1&0&0\\
                1&1&0&1&0\\
                1&0&1&0&1\\
                1&0&0&1&0\\
                }$
            $M_2=\pmatrix{
                0&1&1&0&1\\
                0&0&1&0&0\\
                0&0&0&1&0\\
                1&0&0&0&1\\
                0&0&0&0&0\\
                }$
            $M_3=\pmatrix{
                0&5&10&50&12\\
                5&0&10&0&0\\
                10&10&0&8&0\\
                50&0&8&0&100\\
                12&0&0&100&0\\
                }$

        2. Donner les listes d'adjacence correspondant aux matrices d'adjacence précédentes.
    === "Correction" 


## 3. Algorithmes sur les graphes

### 3.1 Parcours de graphes

Parcourir un graphe, c’est visiter ses différents sommets, afin de pouvoir opérer une action tour à tour sur eux.

Les deux algorithmes fondamentaux permettant de parcourir un graphe s'appellent :

- le parcours en **profondeur d'abord** ;
- le parcours en **largeur d'abord**.

Selon les actions opérées au cours d'un parcours, on peut détecter des cycles dans le graphe, trouver le chemin le plus court entre deux sommets, calculer la distance entre deux sommets, etc.

Un mécanisme crucial dans le parcours de graphes est de marquer les sommets déjà 
visités afin d'éviter de tourner en rond dans le cas où le graphe présente un cycle.

#### Parcours en profondeur d'abord

!!! info "Principe"

    À partir d'un sommet donné, on explore le premier de ses voisins (ou successeurs), 
    puis le premier voisin de celui-ci, et ainsi de suite. Il s’agit pour chaque 
    sommet visité, de choisir un des sommets successeurs du sommet en cours, jusqu’à 
    arriver sur une impasse ou un sommet déjà visité. Dans ce cas, on revient en arrière 
    pour repartir avec un des successeurs non visité du sommet courant.

    Cette façon de faire implique que chaque "branche" est explorée jusqu'au bout, avant de 
    revenir sur nos pas, d'où le nom de **parcours en profondeur**.


!!! danger "Algorithme de parcours en profondeur"

    - On choisit un sommet de départ  
    - On l'empile  
    - Tant que la pile n'est pas vide :  
        - On dépile son sommet  
        - S'il n'a pas encore été visité on le marque et on empile tous ses voisins 
       non encore visités  
        - Sinon, on ne fait rien (on passe donc directement à l'itération suivante)  

En stockant les sommets encore à visiter dans une pile, on s'assure que ce sont les 
derniers sommets découverts qui vont être visités en premier (LIFO, Last In First Out), 
ce qui correspond bien au mécanisme du parcours en profondeur.

!!! example "Implémentation en Python"

    On va ici utiliser l'implémentation d'un graphe avec une liste d'ajacence sous
    la forme d'un dictionnaire. Les paramètres de la fonction `parcours_profondeur`
    sont donc constitués d'un dictionnaire `graphe` et d'une clé de ce dictionnaire
    `debut` correspondant au sommet initial.

    Afin de marquer les sommets déjà visités, nous pouvons utiliser une liste Python,
    mais le type plus approprié dans ce cas est un ensemble (`set` en Python) car dans 
    un ensemble chaque valeur est unique.

    

    ```python
    def parcours_profondeur(graphe : dict, debut) -> set :
        visites = {}            # on définit un ensemble vide
        pile = [debut]
        while len(pile) > 0:
            s = pile.pop()
            if s in visites:    # si s a déjà été visité
                continue        # on passe à l'itération suivante
            visites.add(s)      # sinon l'itération se poursuit en ajoutant s à l'ensemble visites
            for voisin in graphe[s]:
                if voisin not in visites:
                    pile.append(voisin)
        return visites
    ```
#### Parcours en largeur d'abord

!!! info "Principe"

    À partir d'un sommet, on explore tous ses voisins (ou successeurs), puis on explore 
    tous les voisins de ces voisins, et ainsi de suite. Le parcours balaie ainsi chaque 
    "branche" au même rythme, d'où le nom de parcours en largeur.


!!! danger "Algorithme de parcours en largeur"

    - On choisit un sommet de départ
    - On l'enfile
    - Tant que la file n'est pas vide :
        - On défile son premier élément
        - S'il n'a pas encore été visité on le marque et on enfile tous ses voisins non encore visités
        - Sinon, on ne fait rien (on passe donc directement à l'itération suivante)

En stockant les sommets encore à visiter dans une file, on s'assure que ce sont les premiers sommets découverts qui vont être visités en premier (FIFO, First In First Out), cela correspond au parcours en largeur :

!!!example "Implémentation en Python"

    On reprend les mêmes mécanismes que pour le parcours en profondeur, mais en exploitant 
    le type `list` Python à la façon d'une file (on extrait le premier élément de `file` 
    avec `pop(0)`).

    ```python
    def parcours_larg(graphe : dict, debut) -> set :
        visites = {}            # on définit un ensemble vide
        file = [debut]
        while len(file) > 0:
            s = file.pop(0)
            if s in visites:    # si s a déjà été visité
                continue        # on passe à l'itération suivante
            visites.add(s)      # sinon l'itération se poursuit en ajoutant s à l'ensemble
            for voisin in graphe[s]:
                if voisin not in visites:
                    file.append(voisin)
        return visites
    ```

#### Version récursive du parcours en profondeur


Le parcours en profondeur est naturellement récursif. En effet, on part du 
sommet de départ et on explore l'un de ses voisins, puis on explore l'un des voisins du 
voisin, ainsi de suite jusqu'à ce qu'on ne trouve plus de voisins (on arrive à un 
"cul-de-sac"), auquel cas on revient au sommet précédent.

!!! info "Principe"

    On peut traduire l'algorithme de parcours en profondeur de la façon très simple 
    suivante : si un sommet n'est pas visité, on le marque et on parcourt récursivement 
    tous ses voisins.

Comme souvent, l'énoncé d'un programme récursif est plus concis (et plus "logique").

!!! example "Implémentation"

    ```python
    def parcours(graphe : dict , visites : set, s) -> set:
        """parcours en profondeur depuis le sommet s"""
        if s not in visites:
            visites.add(s)
            for voisin in graphe[s]:
                parcours(graphe, visites, voisin)
        return visites
    ```

    Il suffit alors de lancer le premier appel avec un ensemble `visites` vide, ce que fait 
    la fonction d'interface `parcours_prof_rec` suivante.

    ```python
    def parcours_prof_rec(graphe : dict, debut) -> set:
        return parcours(graphe, {}, debut)
    ```


---

Références :

- [C.Gouyou du lycée Marguerite de Valois T1.5_Graphes ](https://cgouygou.github.io/TNSI/T01_StructuresDonnees/T1.5_Graphes/T1.5_Graphes/)
- [Germain BECKER, Lycée Mounier, ANGERS](https://info-mounier.fr/terminale_nsi/algorithmique/algorithmes-graphes)