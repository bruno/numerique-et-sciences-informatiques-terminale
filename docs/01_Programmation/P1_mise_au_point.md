---
author: Bruno Bourgine, Pascal Padilla
title: Mise au point de programme
hide:
 - footer
---

!!! quote "Éléments du référentiel"

    **Contenus** :  

    - Mise au point des programmes. 
    - Gestion des bugs.

    **Attendus**:
    Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs : 

    - problèmes liés au typage, effets de bord non désirés,  
    - débordements dans les tableaux,  
    - instruction conditionnelle non exhaustive,  
    - choix des inégalités,  
    - comparaisons et calculs entre flottants,  
    - mauvais nommage des variables, etc.


!!! info "Pourquoi ce chapitre ?"

    Le développement logiciel est une partie importante de l'informatique. Comme on l'imagine, il s'agit d'écrire du code : de **programmer**. C'est une activité technique qui demande de connaître son langage de programmation.
    
    Mais est-ce suffisant ? Est ce que cela suffit pour être un bon, un très bon programmeur ?
    
    Nous allons voir dans ce chapitre les raisons qui sont au cœur du **génie logiciel** et les techniques indispensables de **bonnes pratiques de programmation**.



## 1. Coder proprement


À partir des expériences et des réflexions d'une poignée de spécialistes du développement logiciel, différentes méthodes sont apparues. Par exemple la [méthode agile](https://fr.wikipedia.org/wiki/M%C3%A9thode_agile) ou le *Clean Code* de Robert C. Martin, aka [Uncle Bob](https://fr.wikipedia.org/wiki/Robert_C._Martin).

L'activité informatique est rarement une activité solitaire. Il est très souvent question de travailler en équipe, en groupe : de **collaborer**. Pour cela, il faut arriver à produire du code durablement **lisible**.

Un projet informatique est une construction complexe. Cette complexité s'appuie sur des éléments de plus en plus simples qui s'articulent pour fonctionner ensemble. Pour que le tout fonctionne, il est indispensable que chaque élément soit **correct**. Idéalement, il faut être certain que chaque élément qui compose l'ensemble du projet ne contienne aucune erreur et fait exactement ce pour quoi il a été programmé. Il faut donc arriver à produire du code **correct**.


## 2. Du code lisible

Le code produit doit être lisible. Que ce soit pour le partager avec une équipe ou encore pour le relire quelques semaines ou quelques mois plus tard, le code doit être facile à lire.

En effet, un développeur passe beaucoup beaucoup plus de temps à lire du code, à réfléchir plutôt à écrire du code. L'essentiel de l'activité de programmation est...de la lecture ! On pourra lire à ce sujet l'[article](source/article_bayrhammer.html){target=_blank} de Bayrhammer Klaus [[source](https://bayrhammer-klaus.medium.com/you-spend-much-more-time-reading-code-than-writing-code-bc953376fe19) du 09/2023].



!!! tip "Règle 1"
    Choisir une convention d'écriture et **s'y tenir**.

Il est indispensable dans son code et dans son projet de ne pas changer de règle typographique.


!!! example "Exemple"
    Les définitions suivantes de `a` et `b` sont équivalentes, syntaxiquement correctes et lisibles. 

    ```python
    a=10
    a = 10
    a= 10

    b = [1,2,3,4,5]
    b = [1, 2, 3, 4, 5]
    b = [1 , 2 , 3 , 4 , 5]
    ```

    Même si en Python il y a des conventions d'écriture (par exemple ici, voir la [PEP8 concernant les espaces](https://peps.python.org/pep-0008/#whitespace-in-expressions-and-statements)), il n'y a pas de raison de choisir un style plutôt qu'un autre. En revanche, une fois que vous avez choisi une règle, il faut s'y tenir. Il ne faut pas mélanger les styles. (Et quitte à choisir un style en Python, autant s'appuyer sur la [PEP8](https://peps.python.org/pep-0008/))



!!! tip "Règle 2 - Bien nommer ses variables"
    Le nom des variables doit être explicite sans être trop long.

Il faut choisir des noms de variables qui expliquent ce que contient la variable. Par convention en Python, les variables sont écrites en minuscules et les mots qui la composent sont séparés par un `_`.


!!! example "Exemple"
    Si on manipule des **années** on peut avoir :

    ```python
    # correct 
    annee = 2022
    annee_naissance = 2003

    # PAS correct
    a = 2022  # trop général
    AnneeDeNaissance = 2008  # pas conventionnel en Python
    annee_de_naissance_du_quatrieme_enfant = 2015  # trop long
    ```



!!! tip "Règle 3 - Bien nommer les fonctions"
    Le nom des fonctions doit indiquer clairement ce que fait la fonction.

Le nom des fonctions, sans être trop long, doit indiquer le rôle de la fonction. Sans en lire l'implémentation (le code), la lecture d'un appel à la fonction doit permettre de retrouver naturellement son rôle.


!!! example "Exemple"
    Le nom des fonctions suivantes permet d'imaginer leurs rôles. Ainsi, même sans avoir le code source sous les yeux, il est possible de lire et de comprendre le code.

    ```python
    moyenne([10, 10, 19])
    distance([(2,10), (5,14)])
    sinus_de_radian(50*3.14)
    sinus_de_degre(205)
    compter_lettre('i', "mississipi")
    ```



!!! tip "Règle 4 - Typer toutes les variables et les fonctions"
    
    * Lors de la définition d'une nouvelle variable, il faut indiquer son type.
    * Lors de la définition d'une fonction, typer ses arguments et son éventuelle sortie

Le type d'une variable exprime le **domaine** auquel ses valeurs appartiennent. Les types sont très importants en informatique puisqu'ils expriment des contraintes fortes que doivent respecter les variables.

En informatique il existe de nombreux type : entier, flottant, booléens, tableau d'entier, tableau de chaînes de caractères, dictionnaire dont les clés sont des chaînes de caractères et les valeurs des nombres entiers, couple d'entiers, triplet, n-uplets, etc.


!!! example "Exemple - types des variables"

    Pour utiliser complètement le typage dans Python, il est nécessaire d'importer la bibliothèque `typing`. Voici des exemple de typage de variables :

    ```python

    # les classiques
    annee: int = 2023           # entier
    pi: float = 3.14159         # flottant
    est_malade: bool = False    # booléen


    # les structures linéaires
    from typing import List, Dict, Tuple
    moyennes: List[int] = [10, 10, 19]  # tableau d'entiers
    prenoms: List[str] = ["Alice", "Bob", "Casimir", "Donald"]  # tableau de chaîne
    ages_clients: Dict[str, int] = {"Alice": 18, "Bob": 42} # dictionnaire clé (chaine) valeur (entier)
    point_origine: Tuple[int,int] = (5, 10)     # couple d'entiers
    origine_xyz: Tuple[int, int, int] = (0,0,0) # triplet d'entiers
    ```

En Python, il est aussi possible de typer les fonctions.

!!! example "Exemple - typer une fonction"

    La fonction `repete_chaine` renvoie un certain nombre de fois une chaîne passée en argument. Par exemple `repete_chaine("ab_", 3)` va renvoyer "ab_ab_ab_".

    Cette fonction admet deux arguments : une chaîne de caractère et un entier.
    Et elle renvoie une chaîne de caractère. Voici comment typer une telle fonction :

    ```python
    def repete_chaine(texte: str, n: int) -> str:
        ''' Renvoie la chaîne composée de n fois `texte`.
        Entrée:
            texte (str): chaîne de caractère à répéter
            n (int): nombre de fois que la chaîne est répétée
        Sortie:
            str: chaîne composé de `texte` concaténé `n` fois
        Exemple:
        >>> repete_chaine("ab_", 3)
        'ab_ab_ab_'
        >>> repete_chaine("-|-", 10)
        "-|--|--|--|--|--|--|--|--|--|-"
        '''
        return texte * n
    ```


!!! example "Exemple - Aller plus loin"

    Voici deux exemples avancés. 
    
    Le premier illustre l'utilisation d'un type générique. C'est-à-dire un type qui n'est pas connu à l'avance. 


    ```python
    # pour aller plus loin

    # créer son propre type
    from typing import TypeVar
    T = TypeVar('T')

    def repete(x: T, n: int) -> List[T]:
        '''Renvoie un tableau contenant `n` occurences de `x`
        Entrée:
            * x (T): variable de type quelconque
            * n (int): nombre de fois que x sera présent dans le tableau renvoyé
        Sortie:
            * List[T]: tableau contenant n fois la variable x
        Exemple et test:
        >>> repete("Alice", 5)
        ["Alice", "Alice", "Alice", "Alice", "Alice"]
        >>> repete(3.14, 6)
        [3.14, 3.14, 3.14, 3.14, 3.14, 3.14]
        '''
        return [x]*n
    ```
    
    Le deuxième exemple montre comment faire lorsque la variable a un contenu qui est optionnel. Ainsi, soit la variable contient une valeur d'un type connu, soit la variable est vide et dans ce cas on lui affecte la valeur `None`.

    ```python
    # type utilisant None    
    from typing import Optional
    def premier_element(t: List[int]) -> Optional[int]:
        ''' Renvoie le premier élément d'un tableau non vide et `None` sinon.
        Entrée:
            t (List[int]): tableau vide ou non d'entier
        Sortie:
            Optional[int]: premier élément du tableau s'il existe ou None
        Exemples et tests:
        >>> premier_element([5,10,15,20])
        5
        >>> premier_element([])
        None
        '''
        if len(t) == 0:
            return None
        else:
            return t[0]

    ```


!!! Remarque
    En Python le **typage est dynamique**. C'est-à-dire que c'est au moment de l'interprétation du code que les questions de typage sont abordées. De plus, le **typage est fort**: c'est à dire que le typage impose des contraintes de domaine aux valeurs des variables. 
    
    Ces particularités font que les erreurs de type ne se révèlent *normalement* que lors de l'exécution du code sur des données incompatibles avec le type courant.

    Ceci est un problème puisqu'il faut exécuter le programme sur des données erronées pour lever une erreur.



## 3. Du code correct

Ensuite, il faut que le code produit soit **correct**. Ainsi, une fonction utilisée dans des *bonnes conditions* doit réaliser ce qu'on attend d'elle.

Pour une fonctions, les *bonnes conditions* sont définies par les contraintes des variables d'entrées. Le typage est une façon claire, efficace et lisible de définir en partie ces contraintes.

Le typage des fonctions et des variables améliore la maintenance et la lisibilité du code mais surtout, il permet de lever des erreurs lors de la compilation, sans avoir besoin d'exécuter le programme.

!!! note "Astuce avec VSCodium"

    VSCodium permet d'utiliser le typage en Python pour lever des exceptions/erreurs lorsqu'une incohérence de type est détectée.

    Pour cela il faut appeler la commande `Python: select linter` et choisir `mypy`.

    Ainsi les erreurs de types détectées seront soulignées.


Afin de rendre le code plus sûr, il faut donc préciser du mieux possible les contraintes liées aux fonctions et aux valeurs renvoyées. Pour rendre cela plus lisible, il est nécessaire de documenter les fonctions précisément.



!!! tip "Règle 5 - Documenter les fonctions"
    Il faut documenter les fonctions :

    * Ce que fait la fonction.
    * Détailler les entrées (type et contraintes)
    * Détailler l'éventuelle sortie (type et états attendus)
    * Donner quelques exemples

Toutes les fonctions vues jusqu'à présent ont été documentées comme cela !



!!! tip "Règle 6 - Tester les fonctions"
    Il faut mettre en place des tests qui permettent de vérifier que sur certains exemples, les fonctions font bien ce qui est attendu.


Il est indispensable d'utiliser des tests pour vérifier que, au moins sur un certain nombre de cas, les fonctions implémentées sont correctes.

Pour cela il existe un outil qui utilise la documentation des fonctions pour effectuer des tests : **`doctest`**. Ce dernier interprète toute les lignes de la documentation commençant par `>>>` et vérifie que le résultat obtenu correspond à ce qui est écrit à la ligne suivante de la documentation.

Soit la fonction précédente `repete_chaine` :

```python
    def repete_chaine(texte: str, n: int) -> str:
        ''' Renvoie la chaîne composée de n fois `texte`.
        Entrée:
            texte (str): chaîne de caractère à répéter
            n (int): nombre de fois que la chaîne est répétée
        Sortie:
            str: chaîne composé de `texte` concaténé `n` fois
        Exemple:
        >>> repete_chaine("ab_", 3)
        'ab_ab_ab_'
        >>> repete_chaine("-|-", 10)
        '-|--|--|--|--|--|--|--|--|--|-'
        '''
        return texte * n
```

Dans la documentation de la fonction, il y a dans la partie *Exemple*, deux lignes qui seront interprétées : 

* `>>> repete_chaine("ab_", 3)` et
* `>>> repete_chaine("-|-", 10)` 

`doctest` vérifiera que l'exécution de la fonction sur les arguments `"(ab_",3)` renvoie bien `'ab_ab_ab_'` et celui sur les arguments `("-|-", 10)` renvoie `'-|--|--|--|--|--|--|--|--|--|-'`

Pour utiliser `doctest` il faut importer la fonction `testmode` et l'exécuter :

```python
from doctest import testmod

def repete_chaine(texte: str, n: int) -> str:
    ...

testmod()
```


!!! Remarque
    Ces tests vérifient un seul élément de la fonction. Il sont appelés **tests unitaires**. Puisqu'ils ne s'intéressent pas à l'implémentation, ce sont des **tests en boite noire**.

    D'autres outils de développement permettent de créer des tests couvrant plus de contraintes des fonctions (`pytest` ou `unittest`). Ce sont ces derniers qui sont utilisés pour créer les exercices auto-évalués que nous verrons cette année.





!!! tip "Règle 7 - Programmation défensive"

    La programmation défensive consiste à faire vérifier par le programme (ou la fonction) que les contraintes sur les variables (ou les entrées et la sortie) sont bien vérifiées.


Pour cela, on utilise le mot clé `assert` suivi d'un test renvoyant un booléen. Si le booléen est faux, alors le programme est immédiatement mis en pause et un exception est levée. Si cette exception n'a pas été prévue par le développeur, alors l'exécution du programme s'arrête.

!!! example "Exemple"

    La fonction `repete_chaine` précédente prend en argument un nombre entier `n` qui est le nombre d'occurrence du texte. Mais que se passe-t-il si ce nombre est négatif ? Est ce qu'on accepte un nombre nul ? Est ce que ce cas pose un problème ?

    Si le développeur décide de *ne pas faire confiance* aux appels de la fonction et arrêter toute exécution si ce nombre est négatif, il ajoutera dans la corps de la fonction un `assert` (noter au passage qu'on a précisé la documentation):

    ```python
    def repete_chaine(texte: str, n: int) -> str:
        ''' Renvoie la chaîne composée de n fois `texte`.
        Entrée:
            texte (str): chaîne de caractère à répéter
            n (int): positif ou nul, nombre de fois que la chaîne est répétée
        Sortie:
            str: chaîne composé de `texte` concaténé `n` fois
        Exemple:
        >>> repete_chaine("ab_", 3)
        'ab_ab_ab_'
        >>> repete_chaine("-|-", 10)
        '-|--|--|--|--|--|--|--|--|--|-'
        '''
        assert n >= 0
        return texte * n
    ```

    Désormais, un appel de type `repete_chaine("abc", -5)` ne sera plus possible et l'exécution du programme cessera immédiatement puisque nous n'avons rien écrit pour prendre en compte cette exception de type `AssertionError`.



## 4. Identifier les erreurs

Comme nous venons de le voir, lorsqu'une exception est levée le message d'erreur du terminal détaille le **type d'exception** et donne des indications au développeur afin d'identifier l'erreur.

Voici une petite liste d'exception classiques en Python :


| Exception | Contexte |
|:----------|:----------------------------|
|`AssertionError` | une clause assert est fausse |
|`NameError`  | accès à une variable inexistante |
|`IndexError` | accès à une variable inexistante
|`KeyError`   | accès à une clé inexistante d'un dictionnaire |
|`ZeroDivisionError` | division par zéro |
|`TypeError`  | opération appliquée à des valeurs incompatibles |


!!! example "Exemple d'erreur de type"

    L'addition d'un entier et d'un tableau n'est pas possible en Python.

    ```
    >>> 1 + [2, 3]
    Traceback (most recent calI last):  File Il <stdin> Il , line 1, in <module>
    TypeError: unsupported operand type(s) for +: 'int' and 'list'
    ```
    
!!! note "Pile d'appels"

    Lorsqu'une exception est levée et n'est pas récupérée par la fonction, elle est transmise à la fonction appelante (ie. la fonction qui a appelé la fonction générant l'exception). Si l'exception n'est toujours pas récupérée par la fonction appelante, alors elle est transmise à son tour à la fonction appelant la fonction appelante. Et ainsi de suite jusqu'à n'être jamais récupérée. 
    
    L'exécution du programme s'interrompt et un message d'erreur affiche la pile d'appels des fonctions appelantes et le type de l'exception.

    Ces information sont essentielles pour comprendre le contexte d'exécution du programme qui a généré cette exception. Le développeur pourra ainsi tenter de corriger son implémentation.
