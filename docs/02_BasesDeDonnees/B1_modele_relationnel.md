---
author: Bruno Bourgine
title: Modèle Relationnel
hide:
 - footer
---

En classe de de première vous avez eu l'occasion de travailler sur des données structurées 
en les stockant dans des fichiers au format CSV. Même si cette méthode de stockage de 
l'information peut s'avérer pratique dans certains cas précis, elle atteint vite des limites 
lorsque la taille et le nombres de tables augmentent. Sans compter que les recherches complexes
 deviennent rapidement fastidieuses à programmer.

!!! question "Problématique"

      Comment faire pour gérer (enregistrer, questionner, sécuriser) un grand nombre de données ?

## 1. Un modèle logique

### 1.1 Historique

!!! info inline end ""

    ![Edgar Cobb](https://upload.wikimedia.org/wikipedia/en/5/58/Edgar_F_Codd.jpg)

!!! info ""

      Le modèle relationnel a été inventé par **Edgar Codd**, ingénieur travaillant pour IBM, 
      dans les années 1970. 

      Son travail théorique (mathématique) a prouvé la forte cohérence de son modèle ainsi 
      que sa puissance.

      L'implémentation de ce modèle quelques années plus tard a donné lieu à un outil robuste 
      et efficace qui a dominé le monde professionnel à partir des années 1980.

### 1.2 Principes

Le modèle relationnel est une façon de représenter et de manipuler des données

 Ce modèle se fonde sur quelques principes essentiels

- une assise mathématique forte sur la théorie des ensembles  
- une implémentation informatique très proche des mathématiques  
- une formulation des énoncés sous forme de termes **logiques**  
- une ensemble d'opérations élémentaires que l'on peut enchaîner pour créer des requêtes 
complexes  

Le modèle relationnel est une manière de modéliser les relations existantes entre plusieurs
informations et de les ordonner entre elles.

Un modèle relationnel est donc basé sur des relations, terme que nous allons définir dans 
la partie suivante.

!!! warning "Modèle de données, structure de données"

    Attention, le chapitre *modèle de données* n'est pas lié au chapitre *structure de données* :

    - structure de données : s'intéresse à la façon d'organiser les données en machine (tableaux, 
     piles, files, objets, etc.)
  
    - modèle de données : s'intéresse à l'information contenue dans ces données. 
      
    Ce sont donc deux niveaux d'**abstractions** différents. 
    Le modèle de donnée est d'un niveau d'abstraction plus élevé.


### 1.3 Exemple : centre de loisir

Pour illustrer ce cours, nous allons prendre l'exemple d'une structure de loisir et d'animation qui 
propose diverses activités à ses membres. La gestion d'une telle structure implique par exemple de 
manipuler des ensembles de personnes (membres, responsables, ...),  des ensembles de matériel 
(salles d'activités, objets, ...) mais aussi des ensembles de créneaux horaires.
 
 Le fonctionnement de la structure repose ainsi sur la mise en relation de tous ces personnes, 
 matériels et créneaux horaires.


## 2. Schéma relationnel

!!! example "Exemple : les activités proposées"

      Si l'on considère l'exemple choisit, l'ensemble des activités peut être représenté ainsi :

      ```
      Activité = {
            ("Aïkido", "adulte", 20, Vrai),
            ("Aïkido", "enfant", 16, Vrai),
            ("Boxe", "7-11", 10, Vrai),
            ("Capoiera", "4-12", 12, Faux),
            ("Chant", "adulte", 1, Faux),
            ("Danse jazz", "6-8", 16, Faux),
            ("Guitare", "8-12", 4, Faux),
            ...
      }
      ```

      On désigne un tel ensemble par le terme **relation**.

      Pour le moment cette relation est loin d'être optimale.

!!! info "Relation"

      Les différents éléments d'une relation s'appellent des **enregistrements** (ou **tuple**, ou
      **entité**, ou **n-uplet**, ou **t-uplet**). Les enregistrements d'une relation possèdent les
      mêmes composantes, que l'on appelle les **attributs** de la relation.

      Une relation est donc un ensemble d'enregistrements, tous de même taille.

      Une relation se conforme toujours à un **schéma** qui est une description indiquant pour 
      chaque attribut de la relation son **nom** et son **domaine** (à savoir le « type » de 
      l'attribut : un entier, une chaîne de caractères, une date, etc.)

!!! example "Attributs de la relation `Activité`"

      Pour le moment la relation `Activité` possède 4 attributs :

      - `intitule` : l'intitulé de l'activité, une chaîne de caractères
      - `public` : le public auquel se destine l'activité, une chaîne de caractères
      - `effectif` : l'effectif maximum d'usages pouvant pratiquer l'activité, un entier naturel
      - `licence` : nécessité de prendre une licence auprès d'une fédération, un booléen
      

      On peut noter ainsi le schéma de la relation `Activité` :
      
      `Activité`(*intitule* `TEXT`, *public* `TEXT`, *effectif* `INT`, *licence* `BOOL`)

Il est courant de représenter une relation sous forme de table, on utilise d'ailleurs utilise 
souvent de manière équivalente les deux termes : *relation* ou *table*.


??? example "Table de la relation `Activité`"

      |intitule|public|effectif|licence|
      |:---:|:---:|:---:|:---:|
      |Aïkido|adulte|20|Vrai|
      |Aïkido|7-12|16|Vrai|
      |Boxe|7-11|12|Vrai|
      |Capoiera|6-12|16|Faux|
      |Chant|adulte|1|Faux|
      |Danse jazz|6-8|10|Faux|
      |Guitare|8-12|4|Faux|

## 3. Base de données relationnelle     

Une base de données relationnelle est un ensemble de relations. Par exemple, la base de données
de notre centre d'animation ne contiendra pas uniquement la relation `Activité`. Elle peut par 
exemple contenir deux autres relations : `Usager` et `Responsable` qui correspondent respectivement
à l'ensemble des usagers du centre et à l'ensemble des personnes qui encadrent les activités.

Le schéma, ou la structure, d'une base de données relationnelle est l'ensemble des schémas des relations de la base. Ainsi, pour le moment, la structure (ou schéma) de la base de données du centre, est :

!!! example "Structure (incomplète) de la base du centre"

      
      `Activité`(*intitule* `TEXT`, *public* `TEXT`, *effectif* `INT`, *licence* `BOOL`) 

      `Responsable`(...)

      `Salle`(...)

      `Créneaux`(...)
      

## 4. Contraintes

Nous venons de voir que pour modéliser correctement des données il faut veiller à :

 - identifier les entités que l'on souhaite décrire (objets, personnes, actions, etc.), 
  ainsi que leurs attributs;  
 - établir les schémas permettant de modéliser les ensembles d'entités, en veillant à choisir 
  des domaines pertinents pour leurs attributs;  

Pour que la base reste cohérente il faut en outre définir les **contraintes d'intégrité** (domaine, 
relation, référence) de la base de données. Il s'agit de  toutes les propriétés logiques vérifiées
par les données à chaque instant.

### 4.1 Contrainte de domaine

Dans le cas de la modélisation d'une base de données, la façon de noter les domaines n'est pas 
primordiale (`INT` ou `Entier` ou `Int` ou `Integer`, etc. pour désigner un attribut dont les 
valeurs sont des entiers), mais elle le deviendra lorsque l'on créera concrètement les tables en 
base de données car il faudra respecter la syntaxe du système de gestion utilisé.

!!! info " "

      Chaque attribut d'une entité doit correspondre à un type de donnée bien choisi de façon à 
      représenter exactement les valeurs possibles d'un attribut (date, entier, chaîne de caractère,
       ...).

      Il est important de bien penser le domaine de chaque attribut dès le départ.

!!! warning "Choix du domaine"

      Bien que le domaine d'un attribut paraisse assez simple à déterminer, il faut être prudent dans 
      certains cas. Par exemple, si le domaine d'un attribut correspondant à un code postal est `INT`, 
      alors si on enregistre un code postal `05000` alors celui-ci sera converti en `5000` (car `05000` 
      = `5000` pour les entiers), ce qui ne correspond pas à un code postal valide... Il est donc 
      nécessaire de donner le domaine `TEXT` à un code postal.

### 4.2 Contrainte d'entité

Chaque élément d'une relation doit être unique et doit pouvoir être identifié sans ambiguïté. 

Dans l'exemple de la relation `Activité` nous retrouvons des activités portant le même *nom*. Si on 
souhaite les distinguer on doit pour cela réunir plusieurs attributs comme par exemple leur *nom* 
**et** leur *public*.

!!! info "Clé primaire"

      **Définition** : Une **clé primaire** est un attribut (ou une réunion d'attributs) qui permet d'identifier de manière unique un enregistrement d'une relation.

Plutôt que de considérer une réunion d'attribut, qui impose des contraintes que nous rencontrerons 
plus tard, nous allons créer un attribut supplémentaire dans la relation `Activité`. Cet attribut
nommé *id_activite* (de type `INT`) va jouer le rôle de clé primaire (on utilise "id" pour 
"identifiant"). 

Pour symboliser la clé primaire dans le schéma d'une relation, il est de coutume de la souligner. Ainsi, 
notre relation `Activité` a pour schéma :

`Activité`(^^*id_activite*^^ `INT`,*intitule* `TEXT`, *public* `TEXT`, *effectif* `INT`, *licence* 
`BOOL`) 

!!! example "La relation `Responsable`"

      Le centre d'animation doit recueillir certaines informations sur les personnes encadrant les
      activités, comme a minima : un nom, un prénom et une adresse email.

      - Si on choisit le nom ou le prénom comme clé primaire, il sera impossible d'enregistrer deux
      usagers portant le même nom ou portant le même prénom, ce qui n'est pas rare.  
      - De même, si on choisit le couple (nom, prénom) comme clé primaire, cela empêche d'enregistrer 
      des homonymes, ce qui peut très bien arriver également.  
      - Si on choisit l'adresse email comme clé primaire, cela impliquerait que deux usagers ne 
      peuvent pas avoir la même adresse email. Cela peut sembler convenir... mais on se heurterait 
      au cas où un usager ne possède pas d'adresse email.

      Comme pour la relation `Activité`, il semble judicieux de créer une clé primaire artificielle, 
      nommée *id_responsable* pour la relation `Responsable` qui aurait alors pour schéma :

      `Responsable`(^^*id_responsable*^^ `INT`, *nom* `TEXT`, *prenom* `TEXT`, *email* `TEXT`)

!!! example "La relation `Salle`"

      La relation `Salle` doit contenir un identifiant de salle ainsi qu'un nombre maximal de 
      personnes pouvant l'occuper.

      `Salle`(^^*id_salle*^^ `INT`, *effectif_max* `INT`)



### 4.3 Contrainte de référence

Pour établir le planning hebdomadaire du centre d'animation, on aimerait pouvoir définir des 
créneaux qui correspondent à la mise en relation d'une activité avec une salle et un responsable.

!!! example "Schéma de la relation `Creneau`"

      On peut imaginer le schéma suivant pour la relation `Creneau`, qui contient toutes les 
      informations nécessaires :

      `Creneau`(*id_activite* `INT`, *intitule*  `TEXT`, *public* `TEXT`, *effectif* `INT`, 
      *licence* `BOOL`, *id_responsable* `INT`, *nom* `TEXT`, *prenom* `TEXT`, *email* `TEXT`, 
      *id_salle* `INT`, *effectif_max* `INT`, *jour* `TEXT`, *heure_debut* `TEXT`, *heure_fin* 
      `TEXT`)

Cela donnerait une table `Creneau` du genre :

??? example "Exemple de table `Creneau`"

      |intitule|public|effectif|licence|id_responsable|nom|prenom|email|id_salle|effectif_max|jour|heure_debut|heure_fin|
      |:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
      |Aïkido|adulte|20|Vrai|1|Daniele|Lebon|da.lb@mail.com|1|25|lundi|20h30|22h00|
      |Aïkido|7-12|16|Vrai|1|Daniele|Lebon|da.lb@mail.com|1|25|mardi|18h30|20h00|
      |Boxe|7-11|12|Vrai|2|Louis|Dupont|loulou.tape@email.net|1|25|mercredi|14h00|15h00|
      |Capoiera|6-12|16|Faux|4|Jose|Saudade|jo.capo@mail.com|1|25|jeudi|18h00|19h30|
      |Chant|adulte|1|Faux|3|Maria|Avay|avm@lapost.net|3|6|mardi|20h30|21h30|
      |Danse jazz|6-8|10|Faux|6|Patrick|Souaise|d.dancing@mel.com|2|20|mercredi|14h00|15h00|
      |Guitare|8-12|4|Faux|3|Neila|Jeune|neilyoung@truc.com|3|6|samedi|10h30|11h30|

Cette relation n'est pas optimale, nous allons voir comment l'améliorer.


!!! info "Clé étrangère"

      **Définition** : Une **clé étrangère** d'une relation est un attribut qui est clé primaire
      d'une autre relation de la base de données.

Ainsi, la relation `Creneau` donnée plus haut possède trois clés étrangères : *id_activite*, 
*id_responsable* et *id_salle* (qui sont des clés primaires respectives des relations `Activite`
`Responsable` et `Salle`).

!!! question "Quelle clé primaire pour la relation `Creneau` ?"

      Un même responsable pouvant animer plusieurs créneaux, il n'est pas possible d'utiliser les 
      attributs correspondant à la relation `Responsable`, de même une salle est utilisée pour
      différents créneaux. Si une activité n'a lieu qu'une seule fois par semaine, alors 
      *id_activite* peut être choisie comme clé primaire.

!!! warning "Redondance"

      Dans une base de données relationnelle, il faut éviter la redondance des données c'est-à-dire 
      qu'une relation ne doit pas contenir des informations déjà disponibles dans d'autres relations 
      (et de manière générale, éviter que des mêmes informations se retrouvent dans plusieurs 
      enregistrements d'une même relation).

La relation `Creneau` telle que définit plus haut contient beaucoup d'informations redondantes.
En effet : 

- pour faire le lien avec le responsable, il est inutile de garder simultanément les attributs 
*id_responsable*, *nom*, *prenom* et *email* : il suffit de conserver l'attribut *id_responsable*.  
- de même pour faire le lien avec l'activité et la salle , il suffit de conserver respectivement 
les attributs *id_activite* et *id_salle*. 

!!! example "Schéma optimisé de la relation `Creneau`"

      Sachant que l'on peut noter les clés étrangères d'une relation en utilisant un "#", on peut 
      désormais écrire une version satisfaisante de la relation `Creneau` :

      `Creneau`(^^*#id_activite*^^ `INT`, *#id_responsable* `INT`, *#id_salle* `INT`, *jour* `TEXT`, 
      *heure_debut* `TEXT`, *heure_fin* `TEXT`)

!!! note "Remarque"

      La clé *id_activite* est donc à la fois clé primaire et clé étrangère de la relation 
      `Creneau`. La clé *id_responsable* est une clé étrangère mais pas une clé primaire de 
      la relation `Creneau` : cela implique qu'un même responsable peut se trouver plusieurs fois 
      dans la relation `Creneau`.


La redondance des données est considérée comme une anomalie d'une base de données, synonyme d'une 
mauvaise conception de la base. En effet, celle-ci est proscrite pour plusieurs raisons :

- faire apparaître des informations non nécessaires à plusieurs endroits (dans plusieurs relations) 
d'une base de données entraîne un coût en mémoire plus important et des performances moindres;  
- si des corrections doivent être faites, elles doivent être faites à un seul endroit.

!!! info "Traduction de la contrainte de référence"
      
      La cohérence et les relations entre les différentes tables sont assurées par les clés étrangères. 
      Elles permettent de respecter ce qu'on appelle les contraintes de référence :

      - une clé étrangère d'une relation doit nécessairement être la clé primaire d'une autre relation.   
      !!! note ""  
            Cela permet de s'assurer de ne pas ajouter des valeurs fictives ne correspondant pas à des 
            entités connues de la base de données ;  
      
      - un enregistrement ne peut être effacé que si sa clé primaire n'est pas associée à des 
      enregistrements liés dans d'autres relations.  

      - une clé primaire ne peut pas être modifiée si l'enregistrement en question est associé à des
      enregistrements liés dans d'autres tables.
     

### 4.4 Contraintes utilisateurs

Les contraintes de domaines ne sont pas toujours suffisantes pour exprimer les contraintes sur
les attributs d'une relation et l'utilisation que l'on souhaite en faire. Par exemple dans la 
relation `Creneau`, il n'est pas possible de mettre en relation une activité dont l'effectifs
est supérieur à l'effectif maximal de la salle l'accueillant.


## 5. Diagramme

Avec toutes les améliorations apportées, le schéma (ou structure) de la base de données du centre
 est le suivant :

!!! example ""
      `Activité`(*intitule* `TEXT`, *public* `TEXT`, *effectif* `INT`, *licence* `BOOL`)  
      `Responsable`(^^*id_responsable*^^ `INT`, *nom* `TEXT`, *prenom* `TEXT`, *email* `TEXT`)  
      `Salle`(^^*id_salle*^^ `INT`, *effectif_max* `INT`)  
      `Creneau`(^^*#id_activite*^^ `INT`, *#id_responsable* `INT`, *#id_salle* `INT`, *jour* `TEXT`, 
      *heure_debut* `TEXT`, *heure_fin* `TEXT`)  

On peut aussi représenter graphiquement ce schéma par le diagramme suivant :

![diagramme réalisé avec [https://dbdiagram.io](https://dbdiagram.io/d)](res/diagram_schema_base.png)


!!! note "Remarques"

      Dans ce diagramme :

      - les clés primaires sont matérialisées ici par un symbole de clé (et en gras). Mais on 
      trouve aussi parfois l'acronyme *CP*, pour *clé primaire*, ou plus souvent sa version anglaise
      *PK*, pour *primary key*;  
      - les clés étrangères sont matérialisées par un trait marquant les associations entre les 
      différentes relations. Mais on trouve aussi souvent l'acronyme FK (*foreign key*
      traduction de *clé étrangèree **).

--- 

références :

- Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
- G.Lassus (lycée François Mauriac, Bordeaux), [Terminale NSI - Base de données](https://glassus.github.io/terminale_nsi/T4_Bases_de_donnees/4.1_Modele_relationnel/cours/)
- [Germain BECKER, Lycée Mounier, ANGERS](https://info-mounier.fr/terminale_nsi/base_de_donnees/#chap1)