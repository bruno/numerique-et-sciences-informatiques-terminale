---
hide :
 - footer
---

# Numérique et Sciences Informatiques - classe de Terminale

Ce site vise à vous exposer les notions exigées par le
programme d'enseignement de la NSI en classe de terminale.

Les ressources proposées ici viennent en complément des travaux pratiques, 
exercices et autres activités proposées en classe. Ce site vous permettra 
de réviser et de rédiger vos fiches de synthèses mais il ne se veut en 
aucun cas exhaustif. 

De nombreuses ressources proposées ici sont issues de publications sous licence
libre issues la communauté éducative.


### Bibliographie & sitographie :

- [Spécialité Numérique et sciences informatiques](https://www.editions-ellipses.fr/accueil/10445-20818-specialite-numerique-et-sciences-informatiques-lecons-avec-exercices-corriges-terminale-nouveaux-programmes-9782340038554.html#/1-format_disponible-broche),  Balabonski Thibaut, Conchon Sylvain, Filliâtre Jean-Christophe, Nguyen Kim, Éditions Ellipses
- [Numérique et sciences informatiques SPÉCIALITÉ](https://www.editions-hatier.fr/livre/fiches-bac-nsi-tle-generale-specialite-bac-2024-9782401064355) Céline Adobet, Guillaume Connan, Gérard Rozsavolgyi, Laurent Signac, Éditions Hatier
- [Terminale NSI - Lycée François Mauriac - Bordeaux](https://glassus.github.io/terminale_nsi/), Gilles Lassus
- [Informatique au lycée Mounier](https://info-mounier.fr/terminale_nsi/)

