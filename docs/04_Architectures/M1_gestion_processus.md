---
author: Bruno Bourgine
title: Gestion des processus
hide:
 - footer
---

!!! warning "Il ne faut pas confondre programme et processus"

    Un **programme** est un fichier binaire (on dit aussi un exécutable) contenant des instructions machines que seul le processeur peut comprendre.

    Un **processus** est un programme en cours d'exécution, autrement dit le phénomène dynamique lié à l'exécution d'un programme par l'ordinateur.

!!! note ""

    Un processus est donc une instance d'un programme auquel est associé :

     - du code  
     - des données/variables manipulées  
     - des ressources : processeur, mémoire, périphériques d'entrée/sortie (voir 
     paragraphe suivant)

## 1. Ordonnancement des processus

### 1.1 Exécution d'un programme

Lorsque l'on exécute un programme (par exemple en cliquant sur l'icône du fichier 
 ou en renseignant son chemin dans un terminal), le système d'exploitation effectue 
 les actions suivantes :

1. le fichier contenant le programme (l'exécutable) est copié dans la mémoire 
   RAM, à une certaine adresse `a` ;

2. le système d'exploitation écrit la valeur a dans le registre IP (instruction 
   pointer).

Au prochain cycle d'horloge du processeur, ce dernier va alors lire l'instruc-
tion se trouvant à l'adresse a et l'exécuter. Une fois cela fait, il exécutera
ensuite la seconde instruction et ainsi de suite.

On rappelle que l'exécution d'une instruction se décompose elle-même en plusieurs 
sous-étapes effectuées au sein du processeur: 

- le chargement (récupérer l'instruction en mémoire),  
- le décodage (déterminer dans la suite d'octets chargés quelle instruction
ils encodent)  
- l'exécution proprement dite.

### 1.2 Interruptions

Si rien de plus n'est fait, alors la seule chose que l'on peut attendre, c'est que le 
programme s'exécute jusqu'à sa dernière instruction, puis rende la main au système
d'exploitation. Impossible alors de l'interrompre et donc aussi de pouvoir exécuter 
deux programmes en même temps. C'est pour cela que les systèmes d'exploitation
utilise le principe de l'interruption.

Une *interruption* est un signal envoyé au processeur lorsqu'un événement
se produit. Il existe plusieurs types d'interruptions. Certaines sont générées
par le matériel (par exemple, un disque dur signale qu'il a fini d'écrire des
octets, une carte réseau signale que des paquets de données arrivent, etc.).

Lorsque le processeur reçoit une interruption, il interrompt son exécution à
la fin de l'instruction courante et exécute un programme se trouvant à une
adresse prédéfinie. Ce programme reçoit en argument une copie des valeurs
courante des registres, ainsi qu'un code numérique lui permettant de savoir
à quel type d'interruption il fait face. Ce programme spécial s'appelle **le
gestionnaire d'interruption**. 

Parmi les interruptions matérielles, on retrouve les **interruptions d 'horloge**. 
Le processeur génère de lui-même une interruption matérielle à intervalles de 
temps fixe. Ces interruptions d'horloges, alliées au gestionnaire d'interruption, 
sont les pièces essentielles permettant d'exécuter des programmes de façon concurrente.

### 1.3 Vocabulaire

!!! info "Exécutable"

    Un fichier binaire contenant des instructions machines directement
    exécutables par le processeur de la machine.

!!! info "Processus"

    Un processus est le phénomène dynamique qui correspond à l'exécution 
    d'un programme particulier. Le système d'exploitation identifie généralement 
    les processus par un numéro unique. 

Un processus est décrit par:

- l'ensemble de la mémoire allouée par le système pour l'exécution
de ce programme (ce qui inclut le code exécutable copié en mémoire et toutes 
les données manipulées par le programme, sur la pile ou dans le tas;  
- l'ensemble des ressources utilisées par le programme (fichiers ouverts, 
  connexions réseaux, etc.) ;  
- les valeurs stockées dans tous les registres du processeur.

!!! info "Thread (ou tâche)"

    Exécution d'une suite d'instructions démarrée par un processus. 
    
    Deux *processus* sont l'exécution de deux programmes (par exemple, 
    un traitement de texte et un navigateur web). 
    
    Deux *threads* sont l'exécution concurrente de deux suites d'instructions 
    d'un même processus. La différence fondamentale entre *processus* et *thread* 
    est que les processus ne partagent pas leur mémoire, alors que les threads, 
    issus d'un même processus, peuvent accéder aux variables globales du programme 
    et occupent le même espace en mémoire.

!!! info "Exécution concurrente"
    
    Deux processus ou tâches s'exécutent de manière concurrente si les intervalles 
    de temps entre le début et la fin de leur exécution ont une partie commune.

!!! info "Exécution parallèle"

    Deux processus ou tâches s'exécutent en parallèle s'ils s'exécutent au même 
    instant. Pour que deux processus s'éxcutent en parallèle, il faut donc plusieurs 
    processeurs sur la machine.

### 1.4 Ordonnanceur

Le système d'exploitation peut configurer une horloge et le gestionnaire 
d'interruption pour « reprendre la main », c'est-à-dire exécuter du code 
qui lui est propre, à intervalles réguliers. Lorsqu'il s'exécute, il peut, 
entre autres choses, décider à quel programme en cours d'exécution il va 
rendre la main.

!!! warning "États d'un processus"

    Au cours de son existence, un processus peut se retrouver dans trois états :

    - *état élu* : lorsqu'il est en cours d'exécution, c'est-à-dire qu'il obtient l'accès au processeur  
    - *état prêt* : lorsqu'il attend de pouvoir accéder au processeur  
    - *état bloqué* : lorsque le processus est interrompu car il a besoin d'attendre une ressource quelconque (entrée/sortie, allocation mémoire, etc.)

    Le processeur ne peut gérer qu'un seul processus à la fois : le processus élu.

!!! infnoteo "Création"

    Lorsqu'un processus est créé il est dans l'état prêt et attend de pouvoir 
    accéder au processeur (d'être élu). 

!!! note "Élection"

    Lorsqu'il est élu, le processus est exécuté
     par le processeur  mais cette exécution peut être interrompue :

    - soit pour laisser la main à un autre processus (qui a été élu) : dans ce cas, le 
    processus de départ repasse dans l'état prêt et doit attendre d'être élu pour 
    reprendre son exécution
    
    - soit parce que le processus en cours a besoin d'attendre une ressource : dans ce 
    cas, le processus passe dans l'état bloqué.

!!! note "Bloquage"

    Lorsque le processus bloqué finit par obtenir la ressource attendue, il peut théoriquement reprendre son exécution mais probablement qu'un autre processus a pris sa place et est passé dans l'état élu. Auquel cas, le processus qui vient d'être "débloqué" repasse dans l'état prêt en attendant d'être à nouveau élu.

!!! info "Cycle de vie d'un processus"

    ![](res/etats_processus.svg)


Le fait que l'ordonnanceur interrompe un processus et sauve son état s'appelle une commutation de contexte. Afin de pouvoir choisir parmi tous les processus lequel exécuter lors de la prochaine interruption, le système d'exploitation conserve pour chaque processus une structure de données nommée PCB (pour l'anglais Process Control Bloc ou bloc de contrôle du processus).

Le PCB est simplement une zone mémoire dans laquelle sont stockées diverses 
informations sur le processus.

Pour choisir parmi les processus celui auquel il va donner la main, l'ordonnanceur
conserve les PCB dans une structure de donnée, par exemple une *file*.

Pour choisir les processus qui seront élus et leur durée de traitement, des 
algorithmes d'ordonnancement sont utilisés et il en existe plusieurs selon la 
stratégie utilisée. On en présente quelques-uns ci-dessous.

#### Ordonnancement First Come First Served (FCFS)

Principe : Les processus sont ordonnancés selon leur ordre d'arrivée ("premier arrivé, premier servi" en français)

#### Ordonnancement Shortest Job First (SJF)

*Principe* : Le processus dont le temps d'exécution est le plus court est ordonnancé en premier.

#### Ordonnancement Shortest Remaining Time (SRT)

*Principe* : Le processus dont le temps d'exécution restant est le plus court parmi ceux qui restent à exécuter est ordonnancé en premier.

#### Ordonnancement temps-partagé (Round-Robin)

*Principe* : C'est la politique du tourniquet : allocation du processeur par tranche (= quantum `q`) de temps.

Dans ce cas, s'il y a n processus, chacun d'eux obtient le processeur au bout de (n−1)×q unités de temps au plus.

#### Ordonnancement à priorités statiques

*Principe* : Allocation du processeur selon des priorités statiques (= numéros affectés aux processus pour toute la vie de l'application)

## 2. Commande UNIX de gestion des processus

Dans les systèmes UNIX, la commande ```ps``` (pour l'anglais process status ou état des processus) permet d'obtenir des informations sur les processus en cours d'exécution.

```
$ ps -a -u -x
```
![](res/ps-a-u-x.png)

La colonne STAT indique l'état du processus (la première lettre en majuscule). 

- R : *running* ou *runnable*, le processus est dans l'état prêt ou en exécution
(la commande ps ne différencie pas ces deux états) ;

- S : *sleeping*, le processus est en attente.

Les colonnes START et TIME indiquent respectivement l'heure ou la date à la-
quelle le programme a été lancé et le temps cumulé d'exécution du processus
correspondant (c'est-à-dire le temps total pendant lequel le processus était
dans l'état « en exécution»). Enfin, la colonne COMMAND indique la ligne de
commande utilisée pour lancer le programme (elle est tronquée dans notre
exemple pour des raisons de place).

### 2.1 PID et PPID

La commande précédente permet de voir que chaque processus est identifié par un numéro : son PID (pour Process Identifier). Ce numéro est donné à chaque processus par le système d'exploitation.

On constate également que chaque processus possède un PPID (pour Parent Process Identifier), il s'agit du PID du processus parent, c'est-à-dire celui qui a déclenché la création du processus. En effet, un processus peut créer lui même un ou plusieurs autres processus, appelés processus fils.

### 2.2 Arborescence de processus

Sous GNU/Linux il est possible de voir l'arborescence des processus avec la commande `pstree`.

![](res/pstree.png)

### 2.3 La commande `top`

La commande `top` permet de connaître en temps réel la liste des processus, classés par ordre décroissant de consommation de CPU. 

![](res/top.png)

La consommation de CPU est calculée en prenant, sur un intervalle de temps donné, le temps qu'a passé le CPU à traiter le processus en question, et en divisant ce temps par le temps total de la mesure. 

### 2.4 La commande `kill`

La commande `kill` permet de fermer un processus, en donnant son PID en argument.

Exemple : `kill 7303` tuera Codium.

## 3. Interblocage

Exemple d'interblocages (deadlock en anglais) de la vie quotidienne :

![](res/ciseaux.png)


Un processus peut être dans l'état bloqué dans l'attente de la libération d'une ressource.

Ces ressources (l'accès en écriture à un fichier, à un registre de la mémoire...) ne peuvent être données à deux processus à la fois. Des processus souhaitant accéder à cette ressource sont donc en concurrence sur cette ressource. Un processus peut donc devoir attendre qu'une ressource se libère avant de pouvoir y accéder (et ainsi passer de l'état Bloqué à l'état Prêt).

*Problème* : Et si deux processus se bloquent mutuellement la ressource dont ils ont besoin ?

*Exemple* : Considérons 2 processus A et B, et deux ressources R et S. L'action des processus A et B est décrite ci-dessous :

![](res/tab_proc.png)

!!! example "Déroulement des processus A et B :"

    - *A* et *B* sont créés et passent à l'état *Prêt*.  
    - L'ordonnanceur déclare *Élu* le processus *A* (ou bien *B*, cela ne change rien).  
    - L'étape A1 de *A* est réalisée : la ressource *R* est donc affectée à *A*.  
    - L'ordonnanceur déclare maintenant *Élu* le processus *B*. *A* est donc passé à *Prêt* en attendant que son tour revienne.  
    - L'étape B1 de *B* est réalisée : la ressource *S* est donc affectée à *B*.  
    - L'ordonnanceur déclare à nouveau *Élu* le processus *A*. *B* est donc passé à *Prêt* en attendant que son tour revienne.  
    - L'étape A2 de *A* est donc enclenchée : **problème**, il faut pour cela pouvoir accèder à la ressource *S*, qui n'est pas disponible. L'ordonnanceur va donc passer *A* à *Bloqué* et va revenir au processus *B* qui redevient *Élu*.
    - L'étape B2 de *B* est donc enclenchée : **problème**, il faut pour cela pouvoir accèder à la ressource *R*, qui n'est pas disponible. L'ordonnanceur va donc passer *B* à *Bloqué*.

Les deux processus A et B sont donc dans l'état Bloqué, chacun en attente de la libération d'une ressource bloquée par l'autre : ils se bloquent mutuellement.

Cette situation (critique) est appelée interblocage ou deadlock.


## 4. Programmation Concurente en Python

Afin d'illustrer les problématiques d'interblocage dans un cadre plus
contrôlé que dans un système d'exploitation, nous donnons ici une introduction 
à la programmation multithread en Python. Comme nous l'avons
déjà expliqué, un thread est un « sous-processus» démarré par un processus et 
s'exécutant de manière concurrente avec le reste du programme. Le
module threading de la bibliothèque standard Python permet de démarrer
des threads.

Recopier et exécuter ce code.

```python
import threading

def hello (n) :
  for i in range(5):
    print ("Je suis le thread", n, "et ma valeur est" : i)
  print ("______ Fin du Thread ", n)

for n in range(4):
  t = threading.Thread(target=hello, args=[n])
  t.start()
```


!!! Abstract "Sources et bibliographie"

    - G.Lassus (lycée François Mauriac, Bordeaux), [Terminale NSI - Gestion des processus](https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.2_Gestion_des_processus/cours/)  
    -Germain BECKER, Lycée Mounier, ANGERS, [Terminale NSI / Thème 3 : Architectures matérielles, systèmes d'exploitation et réseaux / Chapitre 2](https://info-mounier.fr/terminale_nsi/archi_se_reseaux/gestion-processus-ressources)  
    - Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
