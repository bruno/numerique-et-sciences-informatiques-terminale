def fusion(liste1 : list, liste2 : list) -> list :
    liste_totale = []
    i1 = 0
    i2 = 0
    while i1 < len(liste1) and i2 < len(liste2) :
        if liste1[i1] > liste2[i2] :
            liste_totale.append(liste1[i1])
            i1 += 1
        else :
            liste_totale.append(liste2[i2])
            i1 += 2  
    return liste_totale + liste1[i1:] + liste2[i2:]

def fusion2(liste1 : list, liste2 : list) -> list :
    liste_totale = []
    while len(liste1) > 0 and len(liste2) > 0 :
        if liste1[0] < liste2[0] :
            elt = liste1.pop(0)
            liste_totale.append(elt)
        else :
            elt = liste2.pop(0)
            liste_totale.append(elt)
    return liste_totale + liste1 + liste2

def fusion_rec(liste1 : list, liste2 : list) -> list :
    if len(liste1) == 0 :
        return liste2
    if len(liste2) == 0 :
        return liste1
    if liste1[0] < liste2[0] :
        return liste1[0] + fusion_rec(liste1[1:],liste2)
    return liste2[0] + fusion_rec(liste1,liste2[1:])

def moitie_gauche(tab : list) -> list :
    n = len(tab)
    